package seleniava;

import java.util.regex.Pattern;

import org.openqa.selenium.By;

public class Validate {
//	public static Log log = new Log();

	/**
	 * This method validates given element,
	 * 
	 * Allowed types: id, class, tagname, selector, linktext (can be partial),
	 * xpath.
	 * 
	 * @param elem
	 * @return
	 */
	public static By element(String elem) {
		By objBy = null;
		String[] arElem = new String[2];

		if (elem.contains(":")) {
			int delimiter = elem.indexOf(":");
			arElem[0] = elem.substring(0, delimiter);
			arElem[1] = elem.substring((delimiter + 1), (elem.length()));

			if (!arElem[0].isEmpty() && !arElem[1].isEmpty()) {
				switch (arElem[0].toLowerCase()) {
				case "id":
					objBy = new By.ById(arElem[1]);
					break;
				case "class":
					objBy = new By.ByClassName(arElem[1]);
					break;
				case "name":
					objBy = new By.ByName(arElem[1]);
					break;
				case "tagname":
					objBy = new By.ByTagName(arElem[1]);
					break;
				case "selector":
					objBy = new By.ByCssSelector(arElem[1]);
					break;
				case "linktext":
					objBy = new By.ByPartialLinkText(arElem[1]);
					break;
				case "xpath":
					objBy = new By.ByXPath(arElem[1]);
					break;
				default:
					Error.elemTypeError(elem);
					break;
				}
			} else {
				Error.wrongParamFormatError(elem);
			}
		} else {
			Error.wrongParamFormatError(elem);
		}

		return objBy;
	}
	
	/**
	 * This method validates type of value - command or value.
	 * 
	 * @param val
	 * @return (String)
	 */
	public static String varValue(String val) {
		String value = null;
		
		if (Pattern.compile("[a-zA-Z]{1}[a-zA-Z0-9_-]+\\(.*\\)").matcher(val).find())
			value = "cmd";
		else 
			value = "val";
		
		return value;
	}
	
	/**
	 * This method validates type of line given.
	 * 
	 * @param lineRaw
	 * @return (String)
	 */
	public static String lineType(String lineRaw) {
		String type = null;
		
		if (lineRaw.startsWith("#")) {
			type = "comm";
		} else if (lineRaw.startsWith("if") || lineRaw.startsWith("while") || lineRaw.startsWith("for") || lineRaw.startsWith("foreach")) {
			type = "key";
		} else if (lineRaw.startsWith(".")) {
			type = "var";
		} else if (lineRaw.startsWith("function")) {
			type = "func";
		} else if (Pattern.compile("[a-zA-Z]{1}[a-zA-Z0-9_-]+\\(.*\\)").matcher(lineRaw).find()){
			type = "cmd";
		}
		
		return type;
	}
	
//	public static String paramsType(String param) {
//		String type = null;
//		
//		if (param.startsWith(".")) {
//			type = "var";
//		} else if ()
//		
//		return type;
//	}

}
