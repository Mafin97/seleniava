package seleniava;

public class Variable {
	public String name;
	public String value;
	
	/**
	 * Class constructor.
	 */
	public Variable() {
		name = null;
		value = null;
	}
	
	/**
	 * Class constructor.
	 * 
	 * @param _line
	 */
	public Variable(String _line) {
		name = _line.substring(0, _line.indexOf("="));
		value = _line.substring((_line.indexOf("=") + 1), _line.length());
	}
	
	/**
	 * This method sets value of variable.
	 * 
	 * @param line
	 */
	public void setValue(String line) {
//		value = line.substring((line.indexOf("=") + 1), line.length());
		
	}
	
	/**
	 * This method returns value of variable.
	 * 
	 * @param line
	 * @return
	 */
	public static String getvalue(String line) {
		return line.substring((line.indexOf("=") + 1), line.length());
	}
	//rozkminic klase Variable zeby posiadala name, type, value oraz jak rozpoznawac zmienne przekazywane w parametrze metody
}
