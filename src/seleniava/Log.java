package seleniava;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Log{
	private String newFileName;
	private ArrayList<String> newFileLines = new ArrayList<String>();
	
	
	/**
	 * This method creates new log file.
	 * 
	 * @param extension
	 */
	public void create(String name, String extension) {
		SimpleDateFormat dateTime = new SimpleDateFormat("ddMMyyyy_HHmmss");
		newFileName = name + "_" + dateTime.format(new Date()) + "." + extension;
	}
	
	/**
	 * This method adds line to new file.
	 * 
	 * @param line
	 */
	public void add(String line) {
		SimpleDateFormat dateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		newFileLines.add(dateTime.format(new Date()) + " -> " + line);
	}
	
	/**
	 * This method adds line to new file.
	 * 
	 * @param line
	 */
	public void add(String line, int lineInt) {
		SimpleDateFormat dateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		newFileLines.add(dateTime.format(new Date()) + " -> " + line + " @ ln: " + lineInt);
	}
	
	/**
	 * This method saves file.
	 */
	public void save() {
		try {
			PrintWriter fileOutput = new PrintWriter(newFileName, "UTF-8");
			for (String line : newFileLines) {
				fileOutput.println(line);
			}
			fileOutput.close();
		} catch (FileNotFoundException e) {
			Error.fileNotFoundError(newFileName);
		} catch (UnsupportedEncodingException e) {
		}
	}
	
	/**
	 * This method saves log file when test is terminated with critical error.
	 */
	public void shutdown() {
		add("Test terminated...");
		save();
	}
	
	/**
	 * This method saves log file when test is terminated with critical error.
	 * 
	 * @param line
	 */
	public void shutdown(String line) {
		add(line, Parser.atLine);
		add("Test terminated...");
		save();
	}
	
	public void setStaticLogs() {
//		Validate.log = this;
//		OS.log = this;
//		Commands.log = this;
//		Parser.log = this;
//		IO.log = this;
//		Error.log = this;
		Parser.log = this;
		Msg.log = this;
	}

}
