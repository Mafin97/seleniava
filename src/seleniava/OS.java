package seleniava;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class OS {
	private static String osName = null;
	private static String osArch = null;
	public static String EOL = null;
//	public  static Log log = new Log();
	
//	static {
//		identify();
//	}

	/**
	 * This method returns OS name.
	 * 
	 * @return (String)
	 */
	public static String getName() {
		return osName;
	}
	
	/**
	 * This method returns OS architecture.
	 * 
	 * @return (String)
	 */
	public static String getArchitecture() {
		return osArch;
	}
	
	/**
	 * This method identifies OS name and architecture.
	 */
	public static void identify() {
		String sysVer = System.getProperty("os.name").toLowerCase() + System.getProperty("java.vm.name").toLowerCase();

		if (sysVer.contains("windows") && sysVer.contains("64-bit")) {
			osName = "windows";
			osArch = "64";
		} else if (sysVer.contains("windows") && sysVer.contains("32-bit")) {
			osName = "windows";
			osArch = "32";
		} else if (sysVer.contains("linux") && sysVer.contains("64-bit")) {
			osName = "linux";
			osArch = "64";
		} else if (sysVer.contains("linux") && sysVer.contains("32-bit")) {
			osName = "linux";
			osArch = "32";
		} else if (sysVer.contains("mac os")) {
			osName = "macos";
		} else {
			Error.unknownOSError();
		}
		
		EOL = System.getProperty("line.separator");
		
		if (osArch == null) {
			Msg.console("Detected OS: " + osName + ".");
		} else {
			Msg.console("Detected OS: " + osName + " " + osArch + "-bit.");
		}
//		log.add("Detected OS: " + osName + " " + osArch + "-bit.");
	}

	/**
	 * This method creates path to GeckoDriver according to system name and
	 * architecture.
	 * 
	 * @return (String)
	 */
	private static String getGeckodriverPath() {
		final String geckoDir = "drivers/gecko_driver/";
		String geckoVer = null;

		if (osName.equals("windows") && osArch.equals("64"))
			geckoVer = "win64/geckodriver.exe";
		else if (osName.equals("windows") && osArch.equals("32"))
			geckoVer = "win32/geckodriver.exe";
		else if (osName.equals("linux") && osArch.equals("64"))
			geckoVer = "linux64/geckodriver";
		else if (osName.equals("linux") && osArch.equals("32"))
			geckoVer = "linux32/geckodriver";
		else if (osName.equals("macos"))
			geckoVer = "macos/geckodriver";

		return geckoDir + geckoVer;
	}

	/**
	 * This method creates path to ChromeDriver according to system name.
	 * 
	 * @return (String)
	 */
	private static String getChromedriverPath() {
		final String chromeDir = "drivers/chrome_driver/";
		String chromeVer = null;

		if (osName.equals("windows"))
			chromeVer = "win32/chromedriver.exe";
		else if (osName.equals("linux"))
			chromeVer = "linux64/chromedriver";
		else if (osName.equals("macos"))
			chromeVer = "mac64/chromedriver";

		return chromeDir + chromeVer;
	}

	/**
	 * This method creates WebDriver for given browser. Firefox, Chrome are allowed.
	 * 
	 * @param browser
	 * @return (WebDriver)
	 */
	public static WebDriver createDriver(String browser) {
		WebDriver driver = null;
		File fileEx = null;
		try {
			switch (browser.toLowerCase()) {
			case "firefox":
				fileEx = new File(getGeckodriverPath());
				fileEx.setExecutable(true);
				System.setProperty("webdriver.gecko.driver", getGeckodriverPath());
				driver = new FirefoxDriver();
				break;
			case "chrome":
				fileEx = new File(getChromedriverPath());
				fileEx.setExecutable(true);
				System.setProperty("webdriver.chrome.driver", getChromedriverPath());
				driver = new ChromeDriver();
				break;
			default:
	//			log.shutdown("Unknown browser name.");
				Error.unknownBrowserError();
			}
	
			Msg.console("Driver for " + browser + " created.");
	//		log.add("Driver for " + browser + " created.");
		} catch (IllegalStateException e) {
			Error.unknownDriverError();
		}
		
		return driver;
	}

	/**
	 * This method maximizes current browser window.
	 * 
	 * @param driver
	 */
	public static void maximize(WebDriver driver) {
		driver.manage().window().maximize();
		Msg.console("Window maximized.");
//		log.add("Window maximized.");
	}

	/**
	 * This method pauses test for given time in milliseconds.
	 * 
	 * @param millis
	 */
	public static void pause(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			Error.pauseError();
		}
	}
	
	/**
	 * This method pauses given thread for given time in milliseconds.
	 * 
	 * @param millis
	 * @param th
	 */
	public static void pause(int millis, Thread th) {
		try {
			th.sleep(millis);
		} catch (InterruptedException e) {
			Error.pauseError();
		}
	}

}
