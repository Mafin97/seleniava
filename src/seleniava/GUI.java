package seleniava;

import java.io.File;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class GUI extends Application{
    private static 		ArrayList<String> 	consoleLines 	= new ArrayList<String>();
    private static 		int 				lastConsoleLine = 0;
    private static 		boolean 			isRunning 		= false;
	private static 		boolean				start			= false;
	private static 		boolean				stop			= false;
	
	public static void createGUI(String[] args) {
		launch(args);
	}
	
	public static void addToConsole(String text) {
		consoleLines.add(text + OS.EOL);
	}
	
	@Override
    public void start(Stage stage) {
		GridPane 	grid 				= new GridPane();
		Scene 		scene 				= new Scene(grid, 400, 250);
		Text 		sceneTitle 			= new Text("Seleniava loader");
		HBox 		hbSceneTitle 		= new HBox(10);
		FileChooser fileChooser 		= new FileChooser();
		Button 		fileChooserBtn 		= new Button("Wybierz plik...");
        HBox 		hbFileChooserBtn 	= new HBox(10);
        Button 		loadTestBtn 		= new Button("Uruchom test");
	    HBox 		hbLoadFile 			= new HBox(10);
		TextField 	filePathField 		= new TextField();
		TextArea 	consoleField 		= new TextArea();
		
		Thread update = new Thread() {
	    	public void run() {
	    		while (true) {
	    			if (isRunning) {
		    			for (int i = lastConsoleLine; i < consoleLines.size(); i++) {
		    				consoleField.insertText(consoleField.getLength(), consoleLines.get(i));
		    				lastConsoleLine++;
		    			}
	    			}
	    			OS.pause(50, this);
	    		}
	    	}
	    };
	    
		Thread test = new Thread() {
			public void run() {
				while (true) {
					if (start) {
						start = false;
						isRunning = true;
						consoleField.setText("");
		        		if (!filePathField.getText().equals("") && filePathField.getText().endsWith(".test")) {
		        			String name = filePathField.getText().substring((filePathField.getText().lastIndexOf("\\") + 1), filePathField.getText().indexOf(".test"));
		        			Msg.console("Start of test: " + name + ".");
		        			Parser parser = new Parser(name, OS.createDriver("firefox"));
		        			parser.setFilePath(filePathField.getText());
//		        			parser.setFilePath("C:\\Users\\rmffm\\eclipse-workspace-java\\seleniava\\devTest.test"); //for debug
			        		parser.run();
			        		OS.pause(200, this);
			        		isRunning = false;
			        		consoleLines = new ArrayList<String>();
			        		lastConsoleLine = 0;
		        		} else {
		        			Error.noFilePathError();
		        			OS.pause(100, this);
		        			isRunning = false;
		        		}
					}
	        		OS.pause(200, this);
				}
			}
		};
	    		
        stage.setTitle("Seleniava loader");
        stage.setScene(scene);
        
        scene.getStylesheets().add(GUI.class.getResource("styles.css").toExternalForm());
        
        sceneTitle.setId("title");
        hbSceneTitle.setAlignment(Pos.CENTER);
        hbSceneTitle.getChildren().add(sceneTitle);
        
        fileChooser.setTitle("Wybierz plik...");
        hbFileChooserBtn.setAlignment(Pos.CENTER_LEFT);
        hbFileChooserBtn.getChildren().add(fileChooserBtn);
        
        hbLoadFile.setAlignment(Pos.CENTER);
	    hbLoadFile.getChildren().add(loadTestBtn);
        
	    consoleField.setWrapText(true);
	    consoleField.setId("console");
	    
	    //display start messages and clear array for messages from test
	    for (int i = 0; i < consoleLines.size(); i++) {
			consoleField.insertText(consoleField.getLength(), consoleLines.get(i));
		}
	    consoleLines = new ArrayList<String>();
	    
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        grid.add(hbSceneTitle, 0, 0, 2, 1);
        grid.add(hbFileChooserBtn, 0, 1);
	    grid.add(filePathField, 1, 1);
	    grid.add(hbLoadFile, 0, 2, 2, 1);
	    grid.add(consoleField, 0, 3, 2, 3);
	            
        fileChooserBtn.setOnAction(new EventHandler<ActionEvent>() {
        	@Override
        	public void handle(ActionEvent a) {
        		File fileChooserPath = fileChooser.showOpenDialog(stage);
        		if (fileChooserPath != null) {
            		String fileChooserPathS = fileChooserPath.getAbsolutePath().toString();
            		filePathField.setText(fileChooserPathS);
        		}
        	}
        });
        
        loadTestBtn.setOnAction(new EventHandler<ActionEvent>() {
        	@Override
        	public void handle(ActionEvent a) {
//        		test.start();
//        		update.start();
        		if (!isRunning) {
        			start = true;
        		}
        	}
		});
	    
	    stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
	    	@Override
	        public void handle(WindowEvent we) {
	    		if (test.isAlive()) {
	    			test.stop(); //stop() deprecated
	    		}
	    		if (update.isAlive()) {
	    			update.stop(); //stop() deprecated
	    		}
	    		stage.close();
	        }
	    });
	    
        stage.show();
        test.start();
        update.start();
    }


}
