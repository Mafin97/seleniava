package seleniava;

import java.io.IOException;

public class Error {
	public static Log log = new Log();

	/**
	 * This method displays critical error message and shutdown the app.
	 */
	public static void shutdown() {
		log.shutdown();
		Msg.console("Critical error, restart app.");
//		OS.pause(5000);
//		System.exit(0);
	}

	/**
	 * This method displays critical error message and shutdown the app.
	 * 
	 * @param msg
	 */
	public static void shutdown(String msg) {
		Msg.consoleErr(msg);
		Msg.console("Critical error, restart app.");
		log.shutdown();
//		OS.pause(5000);
//		System.exit(0);
	}
	
	/**
	 * This method displays critical error message when OS name is unknown and shutdown the app.
	 */
	public static void unknownOSError() {
		shutdown("Unknow OS name.");
	}
	
	/**
	 * This method displays critical error message when browser name is unknown and shutdown the app.
	 */
	public static void unknownBrowserError() {
		shutdown("Unknown browser name.");
	}
	
	/**
	 * This method displays critical error message when browser name is unknown and shutdown the app.
	 */
	public static void unknownDriverError() {
		shutdown("Error with WebDriver file.");
	}
	
	/**
	 * This method displays critical error message when command is unknown and shutdown the app.
	 */
	public static void unknownCommandError(String cmdName) {
//		Msg.consoleErr("Unknown command: " + cmdName);
		shutdown("Unknown command: " + cmdName);
	}
	
	/**
	 * This method displays error message when file path wasn't given and shutdown the app.
	 */
	public static void noFilePathError() {
		Msg.console("Wrong or empty file path given.");
	}

	/**
	 * This method displays error message when wrong parameter format is given.
	 */
	public static void wrongParamFormatError(String elem) {
		Msg.consoleErr("Wrong parameter format: " + elem + " - use 'type:value'");
	}
	
	/**
	 * This method displays error message when command returns null.
	 */
	public static void commandReturnsVoidError(String name) {
		Msg.consoleErr("Given command: " + name + " returns nothing.");
	}
	
	/**
	 * This method displays critical error message when wrong number of parameters is given and shutdown app.
	 */
	public static void wrongParamsNumberError(String name) {
		shutdown("Wrong number of parameters given for command: " + name);
	}
	
	/**
	 * This method displays error message when wrong url is given.
	 */
	public static void invalidUrlError(String arg) {
		Msg.consoleErr("Wrong url value: " + arg + "");
	}

	/**
	 * This method displays error message wrong type of element is given.
	 * 
	 * @param elem
	 */
	public static void elemTypeError(String elem) {
		Msg.consoleErr("Wrong type of element: " + elem);
	}

	/**
	 * This method displays error message when driver can't find given element.
	 * 
	 * @param elem
	 */
	public static void findError(String elem) {
		Msg.consoleErr("Can't find: " + elem);
	}

	/**
	 * This method displays error message when given element has no dimensions.
	 * 
	 * @param elem
	 */
	public static void dimensionsError(String elem) {
		Msg.consoleErr("Given element: " + elem + " has no dimensions.");
	}
	
	/**
	 * This method displays error message when driver can't find position of given
	 * element.
	 * 
	 * @param elem
	 */
	public static void positionError(String elem) {
		Msg.consoleErr("Can't find position of given element: " + elem + ".");
	}
	
	/**
	 * This method displays error message when element is out of bounds of viewport.
	 * 
	 * @param elem
	 */
	public static void outOfBoundsError(String elem) {
		Msg.consoleErr("Element: " + elem + " is out of bounds of viewport.");
	}

	/**
	 * This method displays critical error message when file is not found.
	 * 
	 * @param path
	 */
	public static void fileNotFoundError(String name) {
		shutdown("File: " + name + " not found.");
	}

	/**
	 * This method displays critical error message when file is empty.
	 * 
	 * @param path
	 */
	public static void fileEmptyError(String name) {
		shutdown("File: " + name + " is empty.");
	}

	/**
	 * This method displays critical error message when file has wrong extension.
	 * 
	 * @param extension
	 */
	public static void fileExtensionError(String extension) {
		shutdown("Wrong file extension: '." + extension + "'. Only '.test' is allowed.");
	}

	/**
	 * This method displays error message when new file is not initialized.
	 */
	public static void initializeNewFileError() {
		Msg.consoleErr("Initialize new file first. Use newFile().");
	}

	/**
	 * This method displays error message when something interrupt Thread.sleep
	 * method.
	 */
	public static void pauseError() {
		Msg.consoleErr("Something interrupted pause method. If it was important - please run your test again.");
	}
}
