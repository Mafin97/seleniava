package seleniava;

public class Msg {
	public static Log log = new Log();
	
	/**
	 * This method prints message in console and adds line in log.
	 * 
	 * @param msg
	 */
	public static void console(String msg) {
		System.out.println(msg);
		GUI.addToConsole(msg);
		log.add(msg);
	}
	
	/**
	 * This method prints message only in console.
	 * 
	 * @param msg
	 */
	public static void consoleOnly(String msg) {
		System.out.println(msg);
	}
	
	/**
	 * This method prints error message in console and adds line in error log.
	 * 
	 * @param msg
	 */
	public static void consoleErr(String msg) {
		msg = msg + " @ ln: " + Parser.atLine + ".";
		System.out.println(msg);
		GUI.addToConsole(msg);
		log.add(msg, Parser.atLine);
	}

}
