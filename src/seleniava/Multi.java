package seleniava;

import java.util.regex.Pattern;

public class Multi extends Object{
//	private String tString;
//	private int tInt;
//	private boolean tBoolean;
//	private float tFloat;
//	private char tChar;
	
	public Multi() {
	}
	
	public static int parseInt(String val) {
		int output = 0;
		
		if (val != null && isNumeric(val)) {
			output = Integer.parseInt(val);
		} 
		
		return output;
	}
	
	public static boolean isNumeric(String val) {
		boolean isNumeric = false;
		
		if (Pattern.compile("[0-9]{1,}").matcher(val).find()) {
			isNumeric = true;
		}
		
		return isNumeric;
	}

}
