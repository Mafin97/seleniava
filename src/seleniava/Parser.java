package seleniava;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;

public class Parser {
	private static String testName;
	private static ArrayList<String> linesRaw = new ArrayList<String>();
	private static ArrayList<String> linesTypes = new ArrayList<String>();
//	private static ArrayList<String> variablesNames = new ArrayList<String>();
//	private static ArrayList<Command> commands = new ArrayList<Command>();
	private	static ArrayList<Variable> variables = new ArrayList<Variable>();
	private static WebDriver driver;
	public 	static Log log = new Log();
	private static String filePath = null;
	public 	static int atLine = 0;
	public	static float loadTime = 0;
//	private static int lastCommand = 0;
//	private static int lastVariable = 0;
//	private static int lastFunction = 0;

	/**
	 * Class constructor.
	 * 
	 * @param name
	 */
	public Parser(String _testName, WebDriver _driver) {
		testName = _testName;
		driver = _driver;
	}
	
	

	/**
	 * This method returns name of current test.
	 * 
	 * @return (String)
	 */
	public String getName() {
		return testName;
	}
	
	/**
	 * This method returns value of given variable.
	 * 
	 * @param varName
	 * @return (String)
	 */
	public static String getVarValue(String varName) {
		String value = null;
		
		for (Variable variable : variables) {
			if (variable.name.equals(varName))
				value = variable.value;
		}
		
		return value;
	}
	
	/**
	 * This method returns value returned by given command.
	 * 
	 * @param cmd
	 * @return (String)
	 */
	public static String getCommandReturnValue(Command cmd) {
		return parseCommand(cmd);
	}
	
	/**
	 * This method sets file path to file with commands. Required to parsing.
	 * 
	 * @param path
	 */
	public void setFilePath(String path) {
		filePath = path;
	}

	/**
	 * This method loads commands.
	 * 
	 * @param lines
	 */
	public void loadCommands(ArrayList<String> lines) {
		linesRaw = lines;
		Msg.console("Commands loaded.");
//		log.add("Commands loaded.");
	}

	/**
	 * This method displays commands of loaded test.
	 */
	public void showCommands() {
		if (!linesRaw.isEmpty()) {
			for (String command : linesRaw) {
				Msg.consoleOnly(command);
			}
		}
	}
	
	/**
	 * This method scans loaded lines to identify what types are they.
	 */
	private static void scanLinesRaw() {
		if (!linesRaw.isEmpty()) {
			for (String lineRaw : linesRaw) {
				linesTypes.add(Validate.lineType(lineRaw));
			}
			Msg.console("Commands scanned.");
//			log.add("Commands scanned.");
		}
	}
	
	/**
	 * This method parses raw commands from file to Command and Variable objects.
	 */
	private static void parse() {
		Variable varTemp = null;
		Command cmdTemp = null;
		
		for (int i = 0; i < linesRaw.size(); i++) {
			atLine++;
			switch (linesTypes.get(i)) {
			case "cmd":
//				commands.add(new Command(linesRaw.get(i)));
				cmdTemp = new Command(linesRaw.get(i));
				parseCommand(cmdTemp);
				break;
			case "var":
				varTemp = new Variable(linesRaw.get(i));
				parseVariable(varTemp);
				break;
			case "func":
				break;
			}
		}
	}
	
	/**
	 * This method parses variable.
	 * 
	 * @param var
	 */
	private static void parseVariable(Variable var) {
		int definedID = -1;
		
		for (int i = 0; i < variables.size(); i++) {
			if (variables.get(i).name.equals(var.name)) {
				definedID = i;
				break;
			}
		}
		
		if (Validate.varValue(var.value).equals("cmd")) {
			var.value = getCommandReturnValue(new Command(var.value));
//			Msg.consoleOnly(var.value); //temporary for debug
		}
		
		if (definedID < 0) {
			variables.add(var);
		} else {
			variables.get(definedID).value = var.value;
		}
	}
	
	/**
	 * This method parses command.
	 * 
	 * @param cmd
	 * @return
	 */
	private static String parseCommand(Command cmd) {
		String retVal = null;
		
		try {
			Object obj = null;
			Method command = null;
			String className = Commands.class.getName();
			Class<?> cls = Class.forName(className);
			int paramsQuantityUser = cmd.params.length;
			ArrayList<Integer> paramsQuantityCommand = new ArrayList<Integer>();
			Class<?>[] paramTypes = null;
			String methodName = cmd.name;
			
			obj = cls.getDeclaredConstructor(WebDriver.class).newInstance(driver);
			for (Method m : obj.getClass().getDeclaredMethods()) {
				if (m.getName().equals(cmd.name)) {
					paramsQuantityCommand.add(m.getParameterCount());
				}
			}
			
			if (!paramsQuantityCommand.contains(paramsQuantityUser)) {
				Error.wrongParamsNumberError(cmd.name);
			} else {
				paramTypes = new Class[paramsQuantityUser];
				for (int i = 0; i < paramsQuantityUser; i++) {
					paramTypes[i] = String.class;
				}
				command = cls.getDeclaredMethod(methodName, paramTypes);
				command.invoke(obj, cmd.params);
				retVal = String.valueOf(obj.getClass().getDeclaredField("ret").get(obj));
			}
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			Error.unknownCommandError(cmd.name);
//			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		
		return retVal;
	}
	
	
	/**
	 * This method runs all of the commands (including variables and functions).
	 */
	public void run() {
		log.create(testName, "log");
		if (filePath != null) {
			loadCommands(IO.readFile(filePath));
			scanLinesRaw();
			parse();
			Msg.console("End of test.");
			log.save();			
		} else {
			Error.noFilePathError();
		}
		
	}
	

}
