package seleniava;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;

public class IO {
	public static String EOL;
//	public static Log log = new Log();
	private String newFileName;
	private ArrayList<String> newFileLines = new ArrayList<String>();

//	static {
//		System.setProperty("file.encoding", "UTF-8"); // setting proper encoding
//	}

	/**
	 * This method creates new file.
	 * 
	 * @param extension
	 */
	public void newFile(String name, String extension) {
		newFileName = name + "." + extension;
	}

	/**
	 * This method adds line to new file.
	 * 
	 * @param line
	 */
	public void toFile(String line) {
		newFileLines.add(line);
	}

	/**
	 * This method saves file.
	 */
	public void saveFile() {
		if (!newFileName.isEmpty() && !newFileLines.isEmpty()) {
			try {
				PrintWriter fileOutput = new PrintWriter(newFileName, "UTF-8");
			} catch (FileNotFoundException e) {
				Error.fileNotFoundError(newFileName);
			} catch (UnsupportedEncodingException e) {
			}
		} else {
			Error.initializeNewFileError();
		}
	}

	/**
	 * This method reads every line of file and returns them as arraylist.
	 * 
	 * @param file
	 * @return (ArrayList<String>)
	 */
	public static ArrayList<String> readFile(String filePath) {
		File file = new File(filePath);
		ArrayList<String> lines = new ArrayList<String>();

		if (checkExtension(file, "test")) {
			try {
				Scanner sc = new Scanner(file, "utf-8");
				if (!empty(file)) {
					while (sc.hasNextLine()) {
						lines.add(sc.nextLine());
					}
					sc.close();
					Msg.console("File: " + file.getName() + " loaded.");
				} else {
					Error.fileEmptyError(file.getName());
				}
			} catch (FileNotFoundException e) {
				Error.fileNotFoundError(file.getName());
			} catch (NullPointerException e) {
				Error.fileNotFoundError(file.getName());
			}
		} else {
			Error.fileExtensionError(getExtension(file));
		}

		return lines;
	}

	/**
	 * This method returns extension of given file.
	 * 
	 * @param file
	 * @return (String)
	 */
	private static String getExtension(File file) { //przerobic na lastIndexOf
//		String reverseName = new StringBuilder(file.getName()).reverse().toString();
//		String extension = reverseName.substring(0, reverseName.indexOf("."));
//		extension = new StringBuilder(extension).reverse().toString();
		return file.getName().substring(file.getName().lastIndexOf("."));
	}

	/**
	 * This method checks extension of given file.
	 * 
	 * @param file
	 * @param extension
	 * @return (boolean)
	 */
	private static boolean checkExtension(File file, String extension) {
//		return getExtension(file).equalsIgnoreCase(extension);
		return file.getName().endsWith("." + extension);
	}

	/**
	 * This methods checks is given file is empty.
	 * 
	 * @param file
	 * @return (boolean)
	 */
	private static boolean empty(File file) {
		return file.length() == 0 ? true : false;
	}

}
