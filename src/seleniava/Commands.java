package seleniava;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;

public class Commands {
	private WebDriver driver = null;
	public String ret = null;
//	public 	static Log log = new Log();
	
	public Commands() {
	}
	
	public void setWebDriver(WebDriver _driver) {
		driver = _driver;
	}
	
	/**
	 * Class constructor.
	 * 
	 * @param drvr
	 */
	public Commands(WebDriver _driver) {
		driver = _driver;
	}
	
	/**
	 * This method writes given text in app console.
	 * 
	 * @param text
	 */
	public void console(String text) {
		Msg.console(text);
	}
	
	/**
	 * This method pauses test for given time in milliseconds.
	 * 
	 * @param millis
	 */
	public void pause(String millis) {
		OS.pause(Integer.parseInt(millis));
	}

	/**
	 * This method loads given URL in current browser window.
	 * 
	 * @param url
	 */
	public void load(String url) {
		try {
			long time = System.currentTimeMillis();
			driver.get(url);
			Msg.console("Page " + url + " loaded.");
			Parser.loadTime = Float.valueOf(String.valueOf((System.currentTimeMillis() - time) / 1000));
			ret = "true";
		} catch (InvalidArgumentException e) {
			Parser.loadTime = 0;
			ret = "false";
			Error.invalidUrlError(url);
		} catch (WebDriverException e) {
			Parser.loadTime = 0;
			ret = "false";
			Error.invalidUrlError(url);
		}
//		log.add("Page " + url + " loaded.");
	}


	/**
	 * This method closes current tab in browser.
	 */
	public void closeTab() {
		driver.close();
		Msg.console("Current tab closed.");
//		log.add("Current tab closed.");
		
	}

	/**
	 * This method closes current browser.
	 */
	public void quit() {
		driver.quit();
		Msg.console("Current browser closed.");
//		log.add("Current browser closed.");
	}

	/**
	 * This method loads new page in current browser window (ta sama czy nowa karta ?).
	 * 
	 * @param url
	 */
	public void navigateTo(String url) {
		driver.navigate().to(url);
		Msg.console("Page " + url + " loaded.");
//		log.add("Page " + url + " loaded.");
	}

	/**
	 * This method does the same operation as clicking on the forward button of any
	 * browser.
	 */
	public void forward() {
		driver.navigate().forward();
		Msg.console("Forward page loaded"); // znalezc dobre literaly
//		log.add("Forward page loaded"); // znalezc dobre literaly
	}

	/**
	 * This method does the same operation as clicking on the back button of any
	 * browser
	 */

	public void back() {
		driver.navigate().back();
		Msg.console("Back page loaded."); // znalezc dobre literaly
//		log.add("Back page loaded."); // znalezc dobre literaly
	}

	/**
	 * This method reloads page in current browser tab.
	 */
	public void reload() {
		driver.navigate().refresh();
		Msg.console("Current page reloaded.");
//		log.add("Current page reloaded.");
	}

	/**
	 * This method clicks given element. Only IDs, classes and names are allowed.
	 * 
	 * @param elem
	 */
	public void click(String elem) {
		try {
			driver.findElement(Validate.element(elem)).click();
			Msg.console("Element: " + elem + " clicked.");
//			log.add("Element: " + elem + " clicked.");
		} catch (NoSuchElementException e) {
//			log.add("Can't find: " + elem, Parser.atLine);
			Error.findError(elem);
		} catch (ElementNotInteractableException e) {
			Error.findError(elem);
		} catch (NullPointerException e) {}
	}
	
	/**
	 * This method right-clicks given element and then clicks element of context menu with given index (starts with 1).
	 * 
	 * @param elem
	 * @param contextIndex
	 */
	public void clickRight(String elem, String contextIndex) {
		Actions action = new Actions(driver);
		
		try {
			action.moveToElement(driver.findElement(Validate.element(elem)));
			action.contextClick(driver.findElement(Validate.element(elem))).build().perform();

			try {
				Robot rb = new Robot(); //wyrzucic robota do pol klasy
				
				for (int i = 0; i < Multi.parseInt(contextIndex); i++) {
					rb.keyPress(KeyEvent.VK_DOWN);
					rb.keyRelease(KeyEvent.VK_DOWN);
				}
				
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
			} catch (AWTException e) {}
			
			Msg.console("Element: " + elem + " right-clicked. Context menu index: " + contextIndex + " clicked.");
		} catch (NoSuchElementException e) {
			Error.findError(elem);
		} catch (MoveTargetOutOfBoundsException e) { //poprawic aby focus szedl na dany obiekt
			Error.outOfBoundsError(elem);
		} catch (NullPointerException e) {
		}
	}
	
	/**
	 * This method clicks first occurence of <a> element with given href value.
	 * 
	 * @param link
	 */
	public void clickLink(String link) {
		List<WebElement> wElems = null;
		boolean exists = false;
		
		wElems = driver.findElements(By.tagName("a"));
		if (wElems.size() > 0) {
			for (WebElement wElem : wElems) {
				if (wElem.getAttribute("href").equals(link)) {
					wElem.click();
					Msg.console("Link: " + link + " clicked.");
					exists = true;
					break;
				}
			}
			if (!exists) 
				Error.findError(link);
		} else
			Error.findError(link);

		
//		log.add("Element: " + elem + " clicked.");
	}

	/**
	 * This method clicks all occurences of given element - for example all checkboxes on page.
	 * 
	 * @param elem
	 */
	public void clickAll(String elem) {
		List<WebElement> elems = driver.findElements(Validate.element(elem));
		if (elems.size() > 0) {
			for (WebElement wElem : elems) {
				wElem.click();
			}
			Msg.console("All elements: " + elem + " clicked.");
//			log.add("All elements: " + elem + " clicked.");
		} else {
//			log.add("Can't find: " + elem, Parser.atLine);
			Error.findError(elem);
		}
	}

//	Submit doesn't wait for page to load. It's better to use click().
//	/**
//	 * This method submits given submit button/input.
//	 * 
//	 * @param elem
//	 */
//	public void submit(String elem) {
//		try {
//			driver.findElement(Validate.element(elem)).submit();
//			Msg.console("element: " + elem + " submitted.");
////			log.add("element: " + elem + " submitted.");
//		} catch (NoSuchElementException e) {
////			log.add("Can't find: " + elem, Parser.atLine);
//			Error.findError(elem);
//		} catch (NullPointerException e) {
//		}
//	}

	/**
	 * This method clears input or textarea field.
	 * 
	 * @param elem
	 */
	public void clear(String elem) {
		try {
			driver.findElement(Validate.element(elem)).clear();
			Msg.console("Element: " + elem + " cleared.");
//			log.add("Element: " + elem + " cleared.");
		} catch (NoSuchElementException e) {
//			log.add("Can't find: " + elem, Parser.atLine);
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method writes text in input or textarea field.
	 * 
	 * @param elem
	 * @param value
	 */
	public void write(String elem, String value) {
		try {
			driver.findElement(Validate.element(elem)).sendKeys(value);
			Msg.console("'" + value + "' sended to: " + elem);
//			log.add("'" + value + "' sended to: " + elem);
		} catch (NoSuchElementException e) {
//			log.add("Can't find: " + elem, Parser.atLine);
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method returns true if given element exists in code and is visible on
	 * page. If exists in code and is not visible then returns false.
	 * 
	 * @param elem
	 * @return (boolean)
	 */
	public void isVisible(String elem) {
		try {
			boolean isVisible = driver.findElement(Validate.element(elem)).isDisplayed();
			if (isVisible) {
				Msg.console("Element: " + elem + " is visible.");
//				log.add("Element: " + elem + " is visible.");
			} else {
				Msg.console("Element: " + elem + " is not visible.");
//				log.add("Element: " + elem + " is not visible.");
			}
			ret = String.valueOf(isVisible);
		} catch (NoSuchElementException e) {
//			log.add("Can't find: " + elem, Parser.atLine);
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method returns true if given element exists in code and is enabled on
	 * page. If exists in code and is not enabled (e.x. disabled input) then returns
	 * false.
	 * 
	 * @param elem
	 * @return (boolean)
	 */
	public void isEnabled(String elem) {
		try {
			boolean isEnabled = driver.findElement(Validate.element(elem)).isEnabled();
			if (isEnabled) {
				Msg.console("Element: " + elem + " is enabled.");
//				log.add("Element: " + elem + " is enabled.");
			} else {
				Msg.console("Element: " + elem + " is not enabled.");
//				log.add("Element: " + elem + " is not enabled.");
			}
			ret = String.valueOf(isEnabled);
		} catch (NoSuchElementException e) {
//			log.add("Can't find: " + elem, Parser.atLine);
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method returns true if given element is selected. Only works with
	 * checkboxes, select options, radios.
	 * 
	 * @param elem
	 * @return (boolean)
	 */
	public void isSelected(String elem) {
		try {
			boolean isSelected = driver.findElement(Validate.element(elem)).isSelected();
			if (isSelected) {
				Msg.console("Element: " + elem + " is selected.");
//				log.add("Element: " + elem + " is selected.");
			} else {
				Msg.console("Element: " + elem + " is not selected.");
//				log.add("Element: " + elem + " is not selected.");
			}
			ret = String.valueOf(isSelected);
		} catch (NoSuchElementException e) {
//			log.add("Can't find: " + elem, Parser.atLine);
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}
	
	
	public void getLoadTime() {
//		Msg.console("Last url was loaded in: " + Parser.loadTime + " seconds.");
		ret = String.valueOf(Parser.loadTime);
	}

	/**
	 * This method returns URL of current page.
	 * 
	 * @return (String)
	 */
	public void getUrl() {
//		Msg.console("Current page url: " + driver.getCurrentUrl());
		ret = driver.getCurrentUrl();
	}

	/**
	 * This method returns the source code of current page.
	 * 
	 * @return (String)
	 */
	public void getSource() {
		ret = driver.getPageSource();
	}

	/**
	 * This method returns title of current page.
	 * 
	 * @return (String)
	 */
	public void getTitle() {
//		Msg.console("Current page title: " + driver.getTitle());
		ret = driver.getTitle();
	}

	/**
	 * This method returns text inside of given element.
	 * 
	 * @param elem
	 * @return (String)
	 */
	public void getText(String elem) {
		try {
			ret = driver.findElement(Validate.element(elem)).getText();
//			Msg.console("Element:" + elem + " contains: " + ret);
		} catch (NoSuchElementException e) {
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method returns tag name of given element.
	 * 
	 * @param elem
	 * @return (String)
	 */
	public void getTagName(String elem) {
		try {
			ret = driver.findElement(Validate.element(elem)).getTagName();
//			Msg.console("Element: " + elem + " tag name: " + ret);
		} catch (NoSuchElementException e) {
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method returns value of CSS tag in given element.
	 * 
	 * @param elem
	 * @param css
	 * @return (String)
	 */
	public void getCss(String elem, String css) {
		try {
			ret = driver.findElement(Validate.element(elem)).getCssValue(css);
//			Msg.console("CSS selector '" + css + "' in element: " + elem + " has value: " + ret);
		} catch (NoSuchElementException e) {
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method returns value of attribute in given element.
	 * 
	 * @param elem
	 * @param attr
	 * @return (String)
	 */
	public void getAttr(String elem, String attr) {
		try {
			ret = driver.findElement(Validate.element(elem)).getAttribute(attr);
			Msg.console("Attribute '" + attr + "' in element: " + elem + " has value: " + ret);
		} catch (NoSuchElementException e) {
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method returns height of given element.
	 * 
	 * @param elem
	 * @return (int)
	 */
	public void getHeight(String elem) {
		try {
			int height = driver.findElement(Validate.element(elem)).getSize().height;
			if (height != 0) {
				ret = String.valueOf(height);
//				Msg.console("Height of element: " + elem + " is " + ret + "px");
			} else {
				Error.dimensionsError(elem);
			}
		} catch (NoSuchElementException e) {
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method returns width of given element.
	 * 
	 * @param elem
	 * @return (int)
	 */
	public void getWidth(String elem) {
		try {
			int width = driver.findElement(Validate.element(elem)).getSize().width;
			if (width != 0) {
				ret = String.valueOf(width);
//				Msg.console("Width of element: " + elem + " is " + ret + "px");
			} else {
				Error.dimensionsError(elem);
			}
		} catch (NoSuchElementException e) {
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method returns X position of given element.
	 * 
	 * @param elem
	 * @return (int)
	 */
	public void getX(String elem) {
		try {
			int x = -9999;
			x = driver.findElement(Validate.element(elem)).getLocation().x;
			if (x != -9999) {
				ret = String.valueOf(x);
				Msg.console("Position X of element: " + elem + " is " + ret + "px");
			} else {
				Error.positionError(elem);
			}
		} catch (NoSuchElementException e) {
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * This method returns Y position of given element.
	 * 
	 * @param elem
	 * @return (int)
	 */
	public void getY(String elem) {
		try {
			int y = -9999;
			y = driver.findElement(Validate.element(elem)).getLocation().y;
			if (y != -9999) {
				ret = String.valueOf(y);
				Msg.console("Position Y of element: " + elem + " is " + ret + "px");
			} else {
				Error.positionError(elem);
			}
		} catch (NoSuchElementException e) {
			Error.findError(elem);
		} catch (NullPointerException e) {
		}
	}

}
