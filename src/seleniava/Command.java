package seleniava;

public class Command{
	public String name;
	public String[] params;
	
	/**
	 * Class constructor.
	 * 
	 * @param _line
	 */
	public Command(String _line) {
		name = _line.substring(0, _line.indexOf("("));
		String paramsTemp = _line.substring(_line.indexOf("(") + 1, (_line.length() - 1));
		if (!paramsTemp.equals("")) {
			params = paramsTemp.split(",");
		} else {
			params = new String[0];
		}
		checkForVariablesAndCommands();
	}
	
	/**
	 * This method checks if given parameters are variables or commands.
	 */
	private void checkForVariablesAndCommands() {
		for (int i = 0; i < params.length; i++) {
			if (Validate.lineType(params[i]) == null) {
				continue;
			} else if (Validate.lineType(params[i]).equals("var")) {
				params[i] = Parser.getVarValue(params[i]);
			} else if (Validate.lineType(params[i]).equals("cmd")) {
				params[i] = Parser.getCommandReturnValue(new Command(params[i]));
			}
		}
	}

}
