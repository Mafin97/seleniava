package seleniava;

public class Main {
	
	static {
		System.setProperty("file.encoding", "UTF-8");
	}

	public static void main(String[] args) {
		Log log = new Log();
		log.setStaticLogs();
		OS.identify();		
		GUI.createGUI(args);
	}

}
